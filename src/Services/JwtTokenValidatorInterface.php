<?php

namespace Drupal\sso_connector\Services;

/**
 * Provides an interface for validating and generating JWT tokens.
 *
 * This service handles the operations related to JWT, such as
 * token validation and token generation for authenticated users.
 */
interface JwtTokenValidatorInterface {

  /**
   * Validates a JWT token.
   *
   * Validates the given JWT token to ensure it is correct and has not expired.
   *
   * @param string $jwt
   *   The JWT token to validate.
   *
   * @return array
   *   An associative array containing the validation result.
   *   - 'valid': (bool) TRUE if the token is valid, FALSE otherwise.
   *   - 'data': (array) The user data extracted from the token, if valid.
   *   - 'error': (string) The error message if the token is invalid.
   */
  public function validate($jwt);

  /**
   * Generates a JWT token for a user.
   *
   * Generates a secure JWT token based on the provided user data.
   *
   * @param array $userData
   *   An associative array containing the user data to encode into the token.
   * @param int $expiration
   *   The token expiration time (in seconds).
   *
   * @return string
   *   The generated JWT token as a string.
   */
  public function generateTokenForUser(array $userData, int $expiration);

}
