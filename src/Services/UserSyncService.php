<?php

namespace Drupal\sso_connector\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service for synchronizing user data between systems.
 *
 * This service provides functionality to synchronize user data from
 * an external source with the local Drupal user entities.
 */
class UserSyncService implements UserSyncServiceInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration object for sso_connector settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Constructs a new UserSyncService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $configFactory, EntityFieldManager $entity_field_manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->config = $configFactory->get('sso_connector.settings');
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function synchronizeUser(array $userData) {
    if (empty($userData['mail'])) {
      throw new \InvalidArgumentException('Email address is required for user synchronization.');
    }

    $user_storage = $this->entityTypeManager->getStorage('user');
    $users = $user_storage->loadByProperties(['mail' => $userData['mail']]);
    $user = NULL;

    if ($users) {
      $user = reset($users);
      $user->save();
    }

    $userSync = $this->config->get('user_sync');

    if (!$user && $userSync) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->create($userData);
      $user->save();
    }
    else {
      return;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function updateUserData($user, $userData) {
    foreach ($userData as $fieldName => $value) {
      if ($user->hasField($fieldName)) {
        $user->set($fieldName, $value);
      }
    }

    $user->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData($user) {
    $userSync = $this->config->get('user_sync');
    $userData = [];
    if (!$userSync) {
      $userData['mail'] = $user->getEmail();
      return $userData;
    }

    $fieldMappings = $this->getStandardEntityFields();
    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldItemList */
    foreach ($user->getFields() as $fieldName => $fieldItemList) {
      // Check if the field name is in the fieldMappings array.
      if (array_key_exists($fieldName, $fieldMappings)) {
        if ($fieldMappings[$fieldName]) {
          $values = [];
          foreach ($fieldItemList as $item) {
            $values[] = $item->value;
          }
          $userData[$fieldName] = count($values) === 1 ? reset($values) : $values;
        }
      }
    }

    return $userData;
  }

  /**
   * Retrieves modifiable entity user fields.
   *
   * @return array
   *   An associative array user fields.
   */
  public function getStandardEntityFields() {
    $entity_type_id = 'user';
    $modifiableFields = [];

    // Get standard fields.
    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($entity_type_id);

    // Known unmodifiable fields.
    $unmodifiableFields = ['uid', 'uuid', 'langcode'];

    // Filter to get only user entity fields that are modifiable.
    foreach ($field_storage_definitions as $field_name => $storage_definition) {
      if ($storage_definition->getProvider() === $entity_type_id && !in_array($field_name, $unmodifiableFields)) {
        $modifiableFields[$field_name] = $field_name;
      }
    }

    return $modifiableFields;
  }

}
