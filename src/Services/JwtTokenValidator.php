<?php

namespace Drupal\sso_connector\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Class for validating and generating JWT tokens.
 */
class JwtTokenValidator implements JwtTokenValidatorInterface {

  /**
   * The configuration object for sso_connector settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new JwtTokenValidator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('sso_connector.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function validate($jwt) {
    $key = $this->config->get('secret_key');
    $method = 'AES-256-CBC';

    try {
      $decoded = JWT::decode($jwt, new Key($key, 'HS256'));
      $decodedArray = (array) $decoded;

      $iv = base64_decode($decodedArray['iv']);
      $decryptedPayload = openssl_decrypt($decodedArray['data'], $method, $key, 0, $iv);

      $userData = json_decode($decryptedPayload, TRUE);

      return ['valid' => TRUE, 'data' => $userData];
    }
    catch (ExpiredException $e) {
      return ['valid' => FALSE, 'error' => 'Token expired'];
    }
    catch (\Exception $e) {
      return ['valid' => FALSE, 'error' => 'Invalid token'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateTokenForUser(array $userData, $expiration = 3600) {
    $key = $this->config->get('secret_key');
    $idpUrl = $this->config->get('idp_url');
    $method = 'AES-256-CBC';
    $ivLength = openssl_cipher_iv_length($method);
    $iv = openssl_random_pseudo_bytes($ivLength);

    // Encrypt the payload.
    $encryptedPayload = openssl_encrypt(json_encode($userData), $method, $key, 0, $iv);

    // Prepare the JWT payload including the encrypted payload and the IV.
    $payload = [
      'data' => $encryptedPayload,
      'iv' => base64_encode($iv),
      'exp' => time() + $expiration,
      'iss' => $idpUrl,
    ];
    $alg = 'HS256';
    // Encode the payload into a JWT.
    $jwt = JWT::encode($payload, $key, $alg);
    return $jwt;
  }

}
