<?php

namespace Drupal\sso_connector\Services;

use Drupal\user\UserInterface;

/**
 * Interface for UserSyncService.
 *
 * Provides an interface for a service that synchronizes user data between
 * systems.
 */
interface UserSyncServiceInterface {

  /**
   * Synchronizes the user.
   *
   * Creates user entities with the provided data if no matching user is found.
   *
   * @param array $userData
   *   An associative array of user data to synchronize. Expected keys are
   *   'email' and 'username', with additional keys for any other user fields.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown if the user entity cannot be saved.
   */
  public function synchronizeUser(array $userData);

  /**
   * Update user data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   * @param array $userData
   *   An associative array of user data to synchronize. Expected keys are
   *   'email' and 'username', with additional keys for any other user fields.
   */
  public function updateUserData(UserInterface $user, array $userData);

  /**
   * Retreives the user data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   *
   * @return array
   *   An associative array of user data to synchronize. Expected keys are
   *   'email' and 'username', with additional keys for any other user fields.
   */
  public function getUserData(UserInterface $user);

}
