<?php

namespace Drupal\sso_connector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\sso_connector\Services\JwtTokenValidator;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for handling the return path from the Identity Provider.
 */
class SsoController extends ControllerBase {

  /**
   * The JWT token validator service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidator
   */
  protected $jwtTokenValidator;

  /**
   * The user synchronization service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * The kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Constructs a new SsoController.
   *
   * @param \Drupal\sso_connector\Services\JwtTokenValidator $jwt_token_validator
   *   The JWT token validator service.
   * @param \Drupal\sso_connector\Services\UserSyncServiceInterface $user_sync
   *   The user synchronization service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   The kill switch.
   */
  public function __construct(
    JwtTokenValidator $jwt_token_validator,
    UserSyncServiceInterface $user_sync,
    KillSwitch $killSwitch,
  ) {
    $this->jwtTokenValidator = $jwt_token_validator;
    $this->userSync = $user_sync;
    $this->killSwitch = $killSwitch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sso_connector.jwt_token_validator'),
      $container->get('sso_connector.user_sync'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * Handles the return from the Identity Provider.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to send back to the browser.
   */
  public function fromIdp(Request $request) {
    $this->killSwitch->trigger();
    $idpUrl = $this->config('sso_connector.settings')->get('idp_url');
    $token = $request->query->get('token');

    try {
      if (!$token) {
        $this->getLogger('sso_connector')->warning('SSO attempt without a token from IP {ip}.', ['ip' => $request->getClientIp()]);
        return new TrustedRedirectResponse($idpUrl . '/user/login');
      }

      $validationResult = $this->jwtTokenValidator->validate($token);
      if (!$validationResult['valid']) {
        $this->getLogger('sso_connector')->notice('Invalid token received from IP {ip}. Token: {token}', [
          'ip' => $request->getClientIp(),
          'token' => $token,
        ]);
        return new TrustedRedirectResponse($idpUrl . '/user/login?error=invalid_token');
      }

      $userMail = $validationResult['data']['mail'];
      $response = $this->loginUserByEmail($userMail, $validationResult);
      if ($response) {
        return $response;
      }

      // If user sync and login attempts failed, handle as an error.
      $this->getLogger('sso_connector')->critical('Failed to synchronize or log in the user with email {email}, redirecting to error.', ['email' => $userMail]);
      return $this->handleLoginFailure($idpUrl);
    }
    catch (\Exception $e) {
      $this->getLogger('sso_connector')->error('Unexpected error during SSO process: {message}', ['message' => $e->getMessage()]);
      return new TrustedRedirectResponse($idpUrl . '/user/login?error=unexpected_error');
    }
  }

  /**
   * Empty returning path.
   */
  public function returnPath(Request $request) {
    return [];
  }

  /**
   * Attempts to load a user by email and finalize the login.
   *
   * This method loads a user based on the provided email, updates the user's
   * data if necessary, and finalizes the user login. If no user is found or
   * if the login process cannot be finalized, the method returns NULL.
   *
   * @param string $userMail
   *   The user's email address.
   * @param array $validationResult
   *   Validation data including user details.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Returns a redirect response to the front page upon successful login,
   *   or NULL if the user cannot be loaded or logged in.
   */
  protected function loginUserByEmail($userMail, $validationResult) {
    $users = $this->entityTypeManager()->getStorage('user')
      ->loadByProperties(['mail' => $userMail]);
    $user = reset($users);
    $userSync = $this->config('sso_connector.settings')->get('user_sync');
    if (!$user && $userSync) {
      $this->userSync->synchronizeUser($validationResult['data']);
      $users = $this->entityTypeManager()->getStorage('user')
        ->loadByProperties(['mail' => $userMail]);
      $user = reset($users);
    }
    if ($user instanceof UserInterface) {
      if ($userSync) {
        $this->userSync->updateUserData($user, $validationResult['data']);
        $this->getLogger('sso_connector')->info('User data updated for user {user_id} during SSO.', ['user_id' => $user->id()]);
      }
      user_login_finalize($user);
      return $this->redirect('<front>');
    }
    else {
      $this->getLogger('sso_connector')->error('User not found or failed to load during SSO with email {email}.', ['email' => $userMail]);
    }

    return NULL;
  }

  /**
   * Handles unsuccessful login attempts and redirects.
   *
   * This method is called when a user login attempt fails, either because
   * the user could not be loaded or synchronized properly.
   *
   * @param string $idpUrl
   *   The URL to redirect to on error.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Returns a response that redirects the user to the identity provider's
   *   login or error page, indicating a problem with the login process.
   */
  protected function handleLoginFailure($idpUrl) {
    $this->killSwitch->trigger();
    return new TrustedRedirectResponse($idpUrl . '/?error=user_sync_error');
  }

}
