<?php

namespace Drupal\sso_connector\Event;

use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines events for user login and update actions.
 *
 * This class provides event constants and a constructor for creating
 * events related to user login and update processes. It enables modules
 * to react to these user events for extending functionality.
 */
class UserEvents extends Event {

  /**
   * Event dispatched when a user logs in.
   */
  public const LOGIN = 'sso_connector.user.login';

  /**
   * Event dispatched when a user's data is updated.
   */
  public const UPDATE = 'sso_connector.user.update';

  /**
   * User entity associated with the event.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Constructs a new UserEvents.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   */
  public function __construct(UserInterface $user) {
    $this->user = $user;
  }

  /**
   * Retrieves the user entity associated with the event.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity.
   */
  public function getUser(): UserInterface {
    return $this->user;
  }

}
