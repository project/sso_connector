<?php

namespace Drupal\Tests\sso_connector\Functional;

use Drupal\Core\Form\FormState;
use Drupal\sso_connector\Form\IdpConfigForm;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the functionality of the IdpConfigForm.
 */
class IdpConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['sso_connector'];

  /**
   * A simple user with 'access content' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($this->user);
  }

  /**
   * Tests the form ID.
   */
  public function testFormId() {
    $form = new IdpConfigForm($this->createMock(UserSyncServiceInterface::class));
    $this->assertEquals('sso_connector_admin_settings', $form->getFormId());
  }

  /**
   * Tests form submission for various configurations.
   */
  public function testFormSubmission() {
    // Navigate to the form.
    $this->drupalGet('/admin/config/sso_connector/settings');

    // Submit the form with the necessary fields.
    $edit = [
      'role' => '1',
      'idp_url' => 'https://example-idp.com',
      'sp_urls' => 'https://example-idp.com',
      'secret_key' => 'secret123',
      'user_sync' => '1',
    ];
    $this->submitForm($edit, 'Save configuration');

    // Check for the confirmation message.
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
  }

  /**
   * Tests validation handlers.
   */
  public function testFormValidation() {
    $form = [];
    $form_state = new FormState();
    $form_state->setValues([
      'role' => '1',
      'sp_urls' => "fizz",
      'secret_key' => 'secret123',
      'user_sync' => '1',
    ]);

    $user_sync = $this->createMock(UserSyncServiceInterface::class);
    $form_object = new IdpConfigForm($user_sync);
    // Build the form first to populate $form array.
    $form = $form_object->buildForm($form, $form_state);
    $form_object->validateForm($form, $form_state);
    $errors = $form_state->getErrors();
    $this->assertNotEmpty($errors, 'Form validation should catch invalid URLs.');
  }

  /**
   * Simulates the AJAX generation of a secret key.
   */
  public function testAjaxSecretKeyGeneration() {
    $form = new IdpConfigForm($this->createMock(UserSyncServiceInterface::class));
    $form_state = new FormState();
    $response = $form->generateSecretKeyAjax([], $form_state);
    $this->assertArrayHasKey('secret_key', $response, 'AJAX response must update the secret key field.');
  }

}
