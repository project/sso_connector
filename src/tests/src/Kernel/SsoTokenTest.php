<?php

namespace Drupal\Tests\sso_connector\Kernel;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\sso_connector\Controller\SsoController;
use Drupal\sso_connector\Services\JwtTokenValidator;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests the SsoController.
 *
 * @group sso_connector
 */
class SsoTokenTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sso_connector',
    'user',
  ];

  /**
   * The mocked logger channel factory service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerFactory;

  /**
   * The mocked JWT token validator service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidator
   */
  protected $jwtTokenValidator;

  /**
   * The mocked user sync service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * The mocked entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mocked configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('user', ['users_data']);

    // Mock the logger factory.
    $this->loggerFactory = $this->createMock(LoggerChannelFactoryInterface::class);
    $logger = $this->createMock(LoggerInterface::class);
    $this->loggerFactory->method('get')->willReturn($logger);
    $this->jwtTokenValidator = $this->createMock(JwtTokenValidator::class);
    $this->userSync = $this->createMock(UserSyncServiceInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
  }

  /**
   * Test the SsoController::fromIdp method.
   */
  /**
   * Tests handling the return from the Identity Provider with a missing token.
   */

  /**
   * Tests handling the return from the Identity Provider with a missing token.
   */
  public function testTokenMissingOrInvalid() {
    // Mock the configuration object.
    $config = $this->createMock(Config::class);
    $config->method('get')
      ->willReturn(TRUE);

    // Mock the configuration factory service.
    $this->configFactory->method('get')
      ->with('sso_connector.settings')
      ->willReturn($config);

    // Set up the container with mock services.
    $container = new ContainerBuilder();
    $container->set('logger.factory', $this->loggerFactory);
    $container->set('sso_connector.jwt_token_validator', $this->jwtTokenValidator);
    $container->set('entity_type.manager', $this->entityTypeManager);
    $container->set('config.factory', $this->configFactory);
    $container->set('sso_connector.user_sync', $this->userSync);
    \Drupal::setContainer($container);

    // Instantiate the SsoController with mock services.
    $controller = new SsoController(
      $this->loggerFactory,
      $this->jwtTokenValidator,
      $this->entityTypeManager,
      $this->configFactory,
      $this->userSync
    );

    // Test with missing token.
    $request = Request::create('/path', 'GET');
    $response = $controller->fromIdp($request);
    $this->assertInstanceOf(RedirectResponse::class, $response);

    // Test with invalid token.
    $request = Request::create('/path', 'GET', ['token' => 'invalid_token']);
    $jwtTokenValidatorMock = $container->get('sso_connector.jwt_token_validator');
    $jwtTokenValidatorMock->expects($this->atLeastOnce())
      ->method('validate')
      ->willReturn(['valid' => FALSE]);
    $response = $controller->fromIdp($request);
    $this->assertInstanceOf(RedirectResponse::class, $response);

  }

}
