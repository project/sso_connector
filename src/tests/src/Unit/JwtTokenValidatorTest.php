<?php

namespace Drupal\Tests\sso_connector\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\sso_connector\Services\JwtTokenValidator;
use Drupal\Tests\UnitTestCase;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Test JwtTokenValidator service.
 */
class JwtTokenValidatorTest extends UnitTestCase {

  /**
   * The JWT token validator.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidator
   */
  protected $jwtValidator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config_factory = $this->createMock(ConfigFactoryInterface::class);
    $config = $this->createMock(ImmutableConfig::class);
    $config->method('get')->willReturnMap([
      ['secret_key', 'your_secret_key'],
      ['idp_url', 'http://example.com'],
    ]);

    $config_factory->method('get')->willReturn($config);

    $this->jwtValidator = new JwtTokenValidator($config_factory);
  }

  /**
   * Tests token generation for a user.
   */
  public function testGenerateTokenForUser() {
    $userData = ['username' => 'testuser', 'email' => 'test@example.com'];
    $token = $this->jwtValidator->generateTokenForUser($userData);

    // Verifying that a string token is returned.
    $this->assertIsString($token);

    // Further decode and verify if correct data is inside the token.
    $decoded = JWT::decode($token, new Key('your_secret_key', 'HS256'));
    $decodedArray = (array) $decoded;
    $decryptedPayload = openssl_decrypt($decodedArray['data'], 'AES-256-CBC', 'your_secret_key', 0, base64_decode($decodedArray['iv']));
    $payloadData = json_decode($decryptedPayload, TRUE);

    $this->assertEquals($userData, $payloadData);
  }

  /**
   * Validates that a valid token is properly validated.
   */
  public function testValidateValidToken() {
    $userData = ['username' => 'testuser', 'email' => 'test@example.com'];
    $token = $this->jwtValidator->generateTokenForUser($userData);
    $validationResult = $this->jwtValidator->validate($token);

    $this->assertTrue($validationResult['valid']);
    $this->assertEquals($userData, $validationResult['data']);
  }

  /**
   * Tests the validation logic for expired tokens.
   */
  public function testExpiredToken() {
    $userData = ['username' => 'testuser', 'email' => 'test@example.com'];
    // Set the expiration time in the past.
    $token = $this->jwtValidator->generateTokenForUser($userData, -3600);
    $validationResult = $this->jwtValidator->validate($token);

    $this->assertFalse($validationResult['valid'], "The token should be invalid as it is expired.");
    $this->assertEquals('Token expired', $validationResult['error']);
  }

  /**
   * Tests the validation logic for tokens with tampered signatures.
   */
  public function testInvalidTokenSignature() {
    $userData = ['username' => 'testuser', 'email' => 'test@example.com'];
    $token = $this->jwtValidator->generateTokenForUser($userData);
    // Alter the token to simulate a tampered signature.
    $token .= 'tampered';

    $validationResult = $this->jwtValidator->validate($token);

    $this->assertFalse($validationResult['valid']);
    $this->assertEquals('Invalid token', $validationResult['error']);
  }

}
