<?php

namespace Drupal\Tests\sso_connector\Unit\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\sso_connector\Services\UserSyncService;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;

/**
 * Test UserSyncService service.
 *
 * @group sso_connector
 * @coversDefaultClass \Drupal\sso_connector\Services\UserSyncService
 */
class UserSyncServiceTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mocked config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The mocked entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The Moked User sync service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSyncService;

  /**
   * The mocked user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityFieldManager = $this->createMock(EntityFieldManager::class);
    $config_factory = $this->createMock(ConfigFactoryInterface::class);
    $config = $this->createMock(ImmutableConfig::class);
    $config->method('get')->willReturnMap([
      ['secret_key', 'your_secret_key'],
      ['idp_url', 'http://example.com'],
    ]);

    $config_factory->method('get')->willReturn($config);
    $this->user = $this->createMock(UserInterface::class);

    $this->userSyncService = new UserSyncService($this->entityTypeManager, $config_factory, $this->entityFieldManager);

  }

  /**
   * Test synchronizeUser method.
   *
   * @covers ::synchronizeUser
   */
  public function testSynchronizeUser() {
    // Case: Email address missing.
    $this->expectException(\InvalidArgumentException::class);
    $this->userSyncService->synchronizeUser([]);

    // Case: New user should be created.
    $userData = ['mail' => 'test@example.com'];
    $userStorage = $this->createMock(UserStorageInterface::class);
    $userStorage->expects($this->once())
      ->method('loadByProperties')
      ->with(['mail' => 'test@example.com'])
      ->willReturn([]);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('user')
      ->willReturn($userStorage);

    $this->userSyncService->synchronizeUser($userData);

    // Case: Existing user, no new user should be created.
    $userStorage = $this->createMock(UserStorageInterface::class);
    $userStorage->expects($this->once())
      ->method('loadByProperties')
      ->with(['mail' => 'test@example.com'])
      ->willReturn(['existing_user']);
    $this->entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('user')
      ->willReturn($userStorage);

    $this->userSyncService->synchronizeUser($userData);
  }

  /**
   * Test updateUserData method.
   *
   * @covers ::updateUserData
   */
  public function testUpdateUserData() {
    // Mock user object.
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->exactly(2))
      ->method('hasField')
      ->willReturn(TRUE);
    $user->expects($this->exactly(2))
      ->method('set');
    $user->expects($this->once())
      ->method('save');

    // Mock user data.
    $userData = ['field1' => 'value1', 'field2' => 'value2'];

    $this->userSyncService->updateUserData($user, $userData);
  }

  /**
   * Test get standard entity fields method.
   *
   * @covers ::getStandardEntityFields
   */
  public function testGetStandardEntityFields() {
    // Mock field storage definitions.
    $fieldStorageDefinitions = [
      'field_name1' => $this->createMock(FieldStorageDefinitionInterface::class),
      'field_name2' => $this->createMock(FieldStorageDefinitionInterface::class),
    // Unmodifiable field.
      'uid' => $this->createMock(FieldStorageDefinitionInterface::class),
    // Unmodifiable field.
      'uuid' => $this->createMock(FieldStorageDefinitionInterface::class),
    // Unmodifiable field.
      'langcode' => $this->createMock(FieldStorageDefinitionInterface::class),
    ];

    // Set expectations for field storage definitions.
    foreach ($fieldStorageDefinitions as $fieldName => $fieldStorageDefinition) {
      if (!in_array($fieldName, ['uid', 'uuid', 'langcode'])) {
        $fieldStorageDefinition->expects($this->once())
          ->method('getProvider')
          ->willReturn('user');
      }
    }

    // Mock entity field manager.
    $this->entityFieldManager->expects($this->once())
      ->method('getFieldStorageDefinitions')
      ->with('user')
      ->willReturn($fieldStorageDefinitions);

    // Call the method and assert.
    $expectedFields = [
      'field_name1' => 'field_name1',
      'field_name2' => 'field_name2',
    ];
    $this->assertEquals($expectedFields, $this->userSyncService->getStandardEntityFields());
  }

}
