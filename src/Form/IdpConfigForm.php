<?php

namespace Drupal\sso_connector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Identity Provider (IdP) configuration settings.
 *
 * This form allows administrators to configure settings related to the
 * Identity Provider, including roles, URLs, and JWT secret key.
 */
class IdpConfigForm extends ConfigFormBase {

  /**
   * The synchronization users service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * Constructs a new IdpConfigForm object.
   *
   * @param \Drupal\sso_connector\Services\UserSyncServiceInterface $user_sync
   *   The synchronization users service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(UserSyncServiceInterface $user_sync, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($config_factory, NULL);
    $this->setLoggerFactory($logger_factory);
    $this->userSync = $user_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('sso_connector.user_sync'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sso_connector.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sso_connector_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sso_connector.settings');
    $is_idp = $form_state->getValue('role') ?? $config->get('role') == 'idp';
    $secret_key = $config->get('secret_key');

    $form['role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is this the Identity Provider (IdP) site?'),
      '#default_value' => $config->get('role') == 'idp' ? TRUE : FALSE,
      '#ajax' => [
        'callback' => '::changeSecretDescriptionAjax',
        'wrapper' => 'jwt-secret-key-wrapper',
        'progress' => 'none',
      ],
    ];

    $form['idp_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Identity provider (IdP) Url:'),
      '#default_value' => $config->get('idp_url'),
      '#states' => [
        'invisible' => [
          ':input[name="role"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['sp_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Service Providers (SPs) Urls:'),
      '#default_value' => $config->get('sp_urls'),
      '#states' => [
        'visible' => [
          ':input[name="role"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['secret_key_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'jwt-secret-key-wrapper'],
    ];

    $form['secret_key_wrapper']['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $secret_key ? $this->t('Secret key is hidden for security reasons.') : '',
      '#description' => $this->initializeDescriptions((bool) $is_idp),
      '#states' => [
        'readonly' => [
          ':input[name="role"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['secret_key_wrapper']['generate'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate secret key'),
      '#ajax' => [
        'callback' => '::generateSecretKeyAjax',
        'wrapper' => 'jwt-secret-key-wrapper',
        'progress' => 'none',
      ],
      '#states' => [
        'visible' => [
          ':input[name="role"]' => ['checked' => TRUE],
        ],
        // Optionally, hide when not checked.
        'invisible' => [
          ':input[name="role"]' => ['checked' => FALSE],
        ],
      ],
    ];

    if (!$is_idp) {
      $form['secret_key_wrapper']['secret_key']['#default_value'] = $secret_key ? $secret_key : '';
    }

    $form['user_sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use User synchronization?'),
      '#description' => $this->t('By checking this checkbox, the users that exist in the IdP and not in the SPs will be created in these atomatically. This checkbox should be checked in both (SP and IdP), to make synchronization works, due to security.'),
      '#default_value' => $config->get('user_sync'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['secret_key'] != $this->t('Secret key is hidden for security reasons.')) {
      $this->config('sso_connector.settings')
        ->set('secret_key', $values['secret_key'])
        ->save();
    }
    elseif (!$values['role'] && empty($values['idp_url'])) {
      $this->config('sso_connector.settings')
        ->set('secret_key', '')
        ->save();
    }

    $this->config('sso_connector.settings')
      ->set('role', $values['role'] ? 'idp' : 'sp')
      ->set('idp_url', $values['idp_url'])
      ->set('sp_urls', $values['role'] ? $values['sp_urls'] : '')
      ->set('user_sync', $values['user_sync'])
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($form_state->getValue('role') == '1' && $form_state->getValue('sp_urls')) {
      $urls = explode("\n", $form_state->getValue('sp_urls'));
      foreach ($urls as $url) {
        if (!filter_var(trim($url), FILTER_VALIDATE_URL)) {
          $form_state->setErrorByName('sp_urls', $this->t('One or more URLs of the SPs are invalid.'));
        }
      }
    }
  }

  /**
   * AJAX callback to generate a new JWT secret key.
   *
   * This function generates a new random secret key to be used for signing
   * JWT tokens and updates the form element with the new value.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The render array for the JWT secret key form element.
   */
  public function generateSecretKeyAjax($form, $form_state) {
    $bytes = openssl_random_pseudo_bytes(32);
    $secretKey = bin2hex($bytes);
    $form_state->setTemporaryValue('token_generated', $secretKey);
    $form_state->setValue(['secret_key_wrapper', 'secret_key'], $secretKey);
    $form_state->setRebuild(TRUE);
    $form['secret_key_wrapper']['secret_key']['#value'] = $secretKey;

    return $form['secret_key_wrapper'];
  }

  /**
   * AJAX callback to change the secret key description based on the role.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The render array for the JWT secret key form element.
   */
  public function changeSecretDescriptionAjax($form, $form_state) {
    $is_idp = $form_state->getValue('role');
    $form['secret_key_wrapper']['secret_key']['#description'] = $this->initializeDescriptions((bool) $is_idp);
    $form['secret_key_wrapper']['generate']['#attributes'] = ['style' => $is_idp ? 'display: inline-block;' : 'display: none;'];
    return $form['secret_key_wrapper'];
  }

  /**
   * Initialize secret key descriptions.
   *
   * @param bool $is_idp
   *   Boolean that determinate if its idp or sp.
   */
  protected function initializeDescriptions($is_idp) {
    $secret_idp_description = $this->t('If you generate a new secret key, it will only be displayed once here. If you dont know your previous Secret key, push "Generate" to get another one and copy it into the SPs.');
    $secret_sp_description = $this->t('Paste here the Secret generated in you IdP site.');
    return $is_idp ? $secret_idp_description : $secret_sp_description;
  }

}
