<?php

namespace Drupal\sso_connector\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\sso_connector\Services\JwtTokenValidatorInterface;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Handles Single Sign-On (SSO) session checks and redirections.
 *
 * This subscriber listens to kernel request events to manage Single Sign-On
 * (SSO) processes, including redirecting users from Service Providers (SP)
 * to the Identity Provider (IdP) for authentication and handling return
 * requests with JWT tokens.
 */
class CheckIdPSessionSubscriber implements EventSubscriberInterface {

  /**
   * The logger channel factory.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The configuration object for sso_connector settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The JWT token validator service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidatorInterface
   */
  protected $jwtTokenValidator;

  /**
   * The private temporary store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The synchronization users service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * The kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The URL generator service.
   *
   * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * SSO Connector settings cache.
   *
   * @var \Drupal\Core\Config\ImmutableConfig|null
   */
  protected $ssoSettings = NULL;

  /**
   * Constructs a new CheckIdPSessionSubscriber object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\sso_connector\Services\JwtTokenValidatorInterface $jwtTokenValidator
   *   The JWT token validator service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private temporary store factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\sso_connector\Services\UserSyncServiceInterface $user_sync
   *   The synchronization users service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $killSwitch
   *   The kill switch.
   * @param \Symfony\Component\Routing\Generator\UrlGeneratorInterface $url_generator
   *   The URL generator service.
   */
  public function __construct(
    LoggerChannelFactoryInterface $loggerFactory,
    AccountProxyInterface $currentUser,
    ConfigFactoryInterface $configFactory,
    JwtTokenValidatorInterface $jwtTokenValidator,
    PrivateTempStoreFactory $temp_store_factory,
    EntityTypeManagerInterface $entity_type_manager,
    UserSyncServiceInterface $user_sync,
    KillSwitch $killSwitch,
    UrlGeneratorInterface $url_generator,
  ) {
    $this->logger = $loggerFactory->get('sso_connector');
    $this->currentUser = $currentUser;
    $this->config = $configFactory->get('sso_connector.settings');
    $this->jwtTokenValidator = $jwtTokenValidator;
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->userSync = $user_sync;
    $this->killSwitch = $killSwitch;
    $this->urlGenerator = $url_generator;
  }

  /**
   * Handles the redirection from a SP to the IdP.
   *
   * This method checks the user's role and initiates a redirect to the IdP
   * for authentication when necessary. It triggers the kill switch to prevent
   * page caching during the redirection.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event triggered by the kernel request.
   */
  public function onKernelRequest(RequestEvent $event) {
    $this->killSwitch->trigger();
    $request = $event->getRequest();
    $role = $this->config->get('role');
    $referer = $request->headers->get('referer');
    $allowedSpUrls = $this->config->get('sp_urls') ? explode("\n", $this->config->get('sp_urls')) : [];

    // Redirect from SP to IdP to start authentication.
    if ($role === 'sp') {
      $idpUrl = $this->config->get('idp_url');
      $loginPath = $this->urlGenerator->generate('user.login');
      if ($request->getPathInfo() === $loginPath && $this->currentUser->isAnonymous()) {
        $this->logger->info('Redirecting to IdP @url for authentication.', ['@url' => $idpUrl]);
        $event->setResponse(new TrustedRedirectResponse($idpUrl . '/return-path'));
      }
    }
    // Handle the return path from IdP after authentication.
    elseif ($role === 'idp') {
      $tempstore = $this->tempStoreFactory->get('sso_connector');
      $url_to_return = $tempstore->get('return_to_sp');
      if ($url_to_return) {
        $tempstore->delete('return_to_sp');
        $event->setResponse(new TrustedRedirectResponse($url_to_return));
      }
      if (str_contains($request->getPathInfo(), '/return-path') && in_array($referer, $allowedSpUrls)) {
        $this->handleSpReturn($event);
      }
    }
  }

  /**
   * Processes the return from a service provider after authentication.
   *
   * Validates the token received upon user's return from an SP and redirects
   * to the original service provider's URL or handles errors as needed.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event to handle.
   */
  protected function handleSpReturn(RequestEvent $event) {
    $this->killSwitch->trigger();
    $request = $event->getRequest();
    $referer = $request->headers->get('referer');
    if ($this->currentUser->isAuthenticated()) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      $userData = $this->userSync->getUserData($user);
      $jwt = $this->jwtTokenValidator->generateTokenForUser($userData, 3600);
      $tempstore = $this->tempStoreFactory->get('sso_connector');
      $tempstore->set('sso_connector_token', $jwt);
      $allowedSpUrls = explode("\n", $this->config->get('sp_urls'));
      if (in_array($referer, $allowedSpUrls)) {
        $tempstore = $this->tempStoreFactory->get('sso_connector');
        $token = $tempstore->get('sso_connector_token');
        if ($token) {
          $this->validateAndRedirect($token, $referer, $event);
        }
        else {
          $this->redirectTo($event, '/user/login', FALSE);
        }
      }
      else {
        $event->setResponse(new TrustedRedirectResponse($referer));
        return;
      }
    }
    else {
      $this->redirectTo($event, $referer, TRUE);
      return;
    }
  }

  /**
   * Validates the SSO token and manages the redirection or error response.
   *
   * @param string $token
   *   The SSO token to validate.
   * @param string $referer
   *   The URL to redirect the user after validation.
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to set responses.
   */
  protected function validateAndRedirect($token, $referer, RequestEvent $event) {
    $validationResult = $this->jwtTokenValidator->validate($token);
    if ($validationResult['valid']) {
      $this->logger->info('Token validation successful.');
      $event->setResponse(new TrustedRedirectResponse($referer . '/from-idp?token=' . $token));
    }
    else {
      $this->logger->warning('Token validation failed.');
      $this->redirectTo($event, $referer, TRUE);
    }
  }

  /**
   * Redirects to a specified URL or to a login page on error.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The current event.
   * @param string $url
   *   The URL to redirect to.
   * @param bool $isError
   *   Indicates if the redirection is due to an error.
   */
  protected function redirectTo($event, $url, $isError = FALSE) {
    $redirectUrl = $isError ? '/user/login?error=invalid_token&sp_to_return=' . $url : $url;
    $event->setResponse(new RedirectResponse($redirectUrl));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onKernelRequest', 299];
    return $events;
  }

}
