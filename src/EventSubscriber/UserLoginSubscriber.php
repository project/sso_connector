<?php

namespace Drupal\sso_connector\EventSubscriber;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\sso_connector\Event\UserEvents;
use Drupal\sso_connector\Services\JwtTokenValidatorInterface;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Subscribes to user login events to generate a JWT token.
 *
 * This subscriber listens for user login events, generates a JWT token
 * for the authenticated user using the JwtTokenValidator, and stores the
 * token in a private temporary store for subsequent use.
 */
class UserLoginSubscriber implements EventSubscriberInterface, ContainerInjectionInterface {

  /**
   * The private temporary store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The JWT token service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidatorInterface
   */
  protected $jwtTokenValidator;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The user synchronization service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * Constructs a new UserLoginSubscriber.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private temporary store factory.
   * @param \Drupal\sso_connector\Services\JwtTokenValidatorInterface $jwt_token_service
   *   The JWT token service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\sso_connector\Services\UserSyncServiceInterface $user_sync
   *   The user synchronization service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    JwtTokenValidatorInterface $jwt_token_service,
    RequestStack $request_stack,
    UserSyncServiceInterface $user_sync,
  ) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->jwtTokenValidator = $jwt_token_service;
    $this->requestStack = $request_stack;
    $this->userSync = $user_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('sso_connector.jwt_token_validator'),
      $container->get('request_stack'),
      $container->get('sso_connector.user_sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      UserEvents::LOGIN => 'onUserLogin',
    ];
  }

  /**
   * Responds to user login events.
   *
   * Generates a JWT token for the logged-in user and stores it in the
   * private temporary store.
   *
   * @param \Drupal\sso_connector\Event\UserEvents $event
   *   The user login event.
   */
  public function onUserLogin(UserEvents $event) {
    $currentRequest = $this->requestStack->getCurrentRequest();
    $spToReturn = $currentRequest->query->get('sp_to_return');

    if ($spToReturn) {
      $user = $event->getUser();
      $userData = $this->userSync->getUserData($user);
      $jwt = $this->jwtTokenValidator->generateTokenForUser($userData, 3600);
      $tempstore = $this->tempStoreFactory->get('sso_connector');
      $tempstore->set('sso_connector_token', $jwt);
      $tempstore->set('return_to_sp', $spToReturn);
    }
  }

}
