<?php

namespace Drupal\sso_connector\EventSubscriber;

use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\sso_connector\Event\UserEvents;
use Drupal\sso_connector\Services\JwtTokenValidatorInterface;
use Drupal\sso_connector\Services\UserSyncServiceInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to entity events to publish messages to Google Cloud Pub/Sub.
 */
class UserUpdateSubscriber implements EventSubscriberInterface {

  /**
   * The private temporary store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The JWT token service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidatorInterface
   */
  protected $jwtTokenValidator;

  /**
   * The user synchronization service.
   *
   * @var \Drupal\sso_connector\Services\UserSyncServiceInterface
   */
  protected $userSync;

  /**
   * Constructs a new UserLoginSubscriber.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private temporary store factory.
   * @param \Drupal\sso_connector\Services\JwtTokenValidatorInterface $jwt_token_service
   *   The JWT token service.
   * @param \Drupal\sso_connector\Services\UserSyncServiceInterface $user_sync
   *   The user synchronization service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    JwtTokenValidatorInterface $jwt_token_service,
    UserSyncServiceInterface $user_sync,
  ) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->jwtTokenValidator = $jwt_token_service;
    $this->userSync = $user_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('sso_connector.jwt_token_validator'),
      $container->get('sso_connector.user_sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[UserEvents::UPDATE][] = ['onUserUpdate', 800];
    return $events;
  }

  /**
   * Method called when Event occurs.
   *
   * @param \Drupal\sso_connector\Event\UserEvents $event
   *   The event.
   */
  public function onUserUpdate(UserEvents $event) {
    $entity = $event->getUser();
    if ($entity instanceof UserInterface) {
      $tempstore = $this->tempStoreFactory->get('sso_connector');
      $token = $tempstore->get('sso_connector_token');
      if ($token) {
        $user = $event->getUser();
        $userData = $this->userSync->getUserData($user);
        $jwt = $this->jwtTokenValidator->generateTokenForUser($userData, 3600);
        $tempstore->set('sso_connector_token', $jwt);

      }
    }
  }

}
