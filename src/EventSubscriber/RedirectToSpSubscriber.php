<?php

namespace Drupal\sso_connector\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\sso_connector\Services\JwtTokenValidatorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Handles redirections to the SP after authentication at the IdP.
 */
class RedirectToSpSubscriber implements EventSubscriberInterface {

  /**
   * The logger channel factory.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The JWT token validator service.
   *
   * @var \Drupal\sso_connector\Services\JwtTokenValidatorInterface
   */
  protected $jwtTokenValidator;

  /**
   * The configuration object for sso_connector settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;


  /**
   * The private temporary store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a new RedirectToSpSubscriber object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   The private temporary store factory.
   * @param \Drupal\sso_connector\Services\JwtTokenValidatorInterface $jwtTokenValidator
   *   The JWT token validator service.
   */
  public function __construct(
    LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactoryInterface $configFactory,
    PrivateTempStoreFactory $tempStoreFactory,
    JwtTokenValidatorInterface $jwtTokenValidator,
  ) {
    $this->logger = $loggerFactory->get('sso_connector');
    $this->config = $configFactory->get('sso_connector.settings');
    $this->tempStoreFactory = $tempStoreFactory;
    $this->jwtTokenValidator = $jwtTokenValidator;
  }

  /**
   * Responds to kernel response events.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   */
  public function onKernelResponse(ResponseEvent $event) {
    $role = $this->config->get('role');
    if ($role === 'idp') {
      $tempstore = $this->tempStoreFactory->get('sso_connector');
      $urlToReturn = $tempstore->get('return_to_sp');
      if ($urlToReturn) {
        $tempstore->delete('return_to_sp');
        $tempstore = $this->tempStoreFactory->get('sso_connector');
        $token = $tempstore->get('sso_connector_token');
        if ($token) {
          $validationResult = $this->jwtTokenValidator->validate($token);
          if ($validationResult['valid']) {
            $this->logger->info('Token validated successfully.', ['token' => $token]);
            $event->setResponse(new TrustedRedirectResponse($urlToReturn . '/from-idp?token=' . $token));
            return;
          }
          else {
            $this->logger->warning('Token validation failed.', ['token' => $token]);
            $event->setResponse(new RedirectResponse('/user/login?error=invalid_token&sp_to_return=' . $urlToReturn . 'user/login'));
            return;
          }
        }
        else {
          $this->logger->error('No token found for redirection.', ['urlToReturn' => $urlToReturn]);
          $event->setResponse(new RedirectResponse('/user/login'));
          return;
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [KernelEvents::RESPONSE => ['onKernelResponse', -10]];
  }

}
