# SSO Connector Module for Drupal

The SSO Connector module simplifies the integration of Single Sign-On (SSO)
across multiple Drupal sites or applications. It enhances security and user
experience by allowing unified user authentication and automatic user 
synchronization between Identity Providers (IdPs) and Service Providers (SPs).

## Table of Contents

- [Requirements](#requirements)
- [Recommended Modules](#recommended-modules)
- [Installation](#installation)
- [Configuration](#configuration)
- [Troubleshooting](#troubleshooting)
- [FAQ](#faq)
- [Maintainers](#maintainers)

## Requirements

No other modules are required.

## Recommended Modules

- **OAuth**: Provides OAuth consumer and server functionalities.
- **LDAP**: Facilitates integration with LDAP servers for user authentication
and data synchronization.

These modules are not required for the SSO Connector to function but can 
enhance its capabilities and compatibility with other authentication systems.

## Installation

1. Download the SSO Connector module from the Drupal project page.
2. Place the module under the `[DRUPAL_ROOT]/modules/contrib/` directory.
3. Enable the module via Drupal's Extend page or by using Drush:
`drush en sso_connector -y`.

## Configuration

Navigate to `Configuration > System > SSO Connector Settings` to configure the
module:

1. **Identity Provider Settings**: If your site acts as the IdP, specify this
and list all the SPs that will connect to it.
2. **Service Provider Settings**: If your site is an SP, specify the IdP
details here.
3. **Token Configuration**: Generate a secure token for authentication
validation across the IdP and all SPs. You should generate the token in the
IdP site and paste it into the SP configuration sites.
4. **Users synchronization**: If your site requires user synchronization, you
should enable it in both sites (IdP and SP) to make it work.

Ensure you save your configuration after making any changes.

## Troubleshooting

If you encounter issues during the installation or use of the SSO Connector,
check the following:

- Verify that your Drupal installation meets the module's requirements.
- Clear all caches and re-run cron.
- Check the Drupal and server error logs for any relevant messages.
- Ensure that all module dependencies are correctly installed and configured.

## FAQ

**Q: Can I use SSO Connector with Drupal 7?**  
A: No, SSO Connector is only compatible with Drupal 9.x, 10.x and 11.x.

**Q: What should I do if the synchronization is not working?**
A: Check your network settings and ensure that the SSO Connector settings are
correctly configured on both the IdP and SPs. Also, verify that the secure 
token matches across all systems.

## Maintainers

- **[César M. S. Felipe](https://www.drupal.org/u/cesarmsfelipe)** - Initial 
module creator and current maintainer -  

For more assistance, submit an issue on the Drupal project page or contact the
maintainers directly.
